###############
## [Be] Lazy ##
###############
[globalVar = LIT:1 = {$plugin.tx_teufels_thm_webfontloader.settings.production.optional.active}]
page {
    headerData.333014 = TEXT
    headerData.333014.value (
<script type="text/javascript">;(function(){var a=document.createElement("script");var b=document.getElementsByTagName("script")[0];a.src=("https:"==document.location.protocol?"https":"http")+"://"+window.location.hostname+(window.location.port ? ':'+window.location.port : '')+"/typo3conf/ext/teufels_thm_webfontloader/Resources/Public/Assets/Js/webfontloader.min.js.gzip";a.type="text/javascript";a.async="true";b.parentNode.insertBefore(a,b)})();
</script>
    )
}
[global]